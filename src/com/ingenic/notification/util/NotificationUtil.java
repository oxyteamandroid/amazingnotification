/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.notification.util;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.ingenic.iwds.HardwareList;
import com.ingenic.notification.R;
import com.ingenic.notification.dao.NotificationConstants;
import com.ingenic.notification.fragment.AllNewsFragment;
import com.ingenic.notification.fragment.NewsClassifyFragment;
import com.ingenic.notification.fragment.NewsDetailsFragment;

/**
 * FragmenUtil切换时的封装起来的工具类
 *
 * @author tZhang
 */
public class NotificationUtil {

    /**
     * 全部消息
     */
    public static final int ALL_NOTIFICATIONS = 0;

    /**
     * 全部消息的Fragment Tag
     */
    public static final String ALL_NOTIFICATIONS_TAG = "ALL_NOTIFICATIONS_TAG";

    /**
     * 全部分类
     */
    public static final int ALL_CLASSIFICATIONS = 1;

    /**
     * 全部分类的Fragment Tag
     */
    public static final String ALL_CLASSIFICATIONS_TAG = "ALL_CLASSIFICATIONS_TAG";

    /**
     * 某一类消息
     */
    public static final int CLASSIFY_NOTIFICATIONS = 2;

    /**
     * 某一类消息的Fragment Tag
     */
    public static final String CLASSIFY_NOTIFICATIONS_TAG = "CLASSIFY_NOTIFICATIONS_TAG";

    /**
     * 消息详情
     */
    public static final int DETAILS_NOTIFICATION = 3;

    /**
     * 消息详情的Fragment Tag
     */
    public static final String DETAILS_NOTIFICATION_TAG = "DETAILS_NOTIFICATION_TAG";

    /**
     * 切换到tag对应的fragment界面
     *
     * @param flag
     */
    public static void switchToFragment(FragmentManager fragmentManager,
            int flag, Bundle bundle) {

        FragmentTransaction ft = fragmentManager.beginTransaction();

        Fragment fragment = null;

        switch (flag) {
        case ALL_NOTIFICATIONS: // 所有消息列表
            fragment = fragmentManager.findFragmentByTag(ALL_NOTIFICATIONS_TAG);
            if (fragment == null) {
                fragment = new AllNewsFragment(null);
                fragment.setArguments(bundle);
            }
            if (!fragment.isResumed()) {
                ft.replace(R.id.notification_container, fragment,
                        ALL_NOTIFICATIONS_TAG);
            }
            break;

        case ALL_CLASSIFICATIONS: // 所有分类列表
            fragment = fragmentManager
                    .findFragmentByTag(ALL_CLASSIFICATIONS_TAG);
            if (fragment == null) {
                fragment = new NewsClassifyFragment();
                fragment.setArguments(bundle);
            }
            if (!fragment.isResumed()) {
                ft.replace(R.id.notification_container, fragment,
                        ALL_CLASSIFICATIONS_TAG);
            }
            break;

        case CLASSIFY_NOTIFICATIONS: // 某一类消息列表
            fragment = fragmentManager
                    .findFragmentByTag(CLASSIFY_NOTIFICATIONS_TAG);
            if (fragment == null) {
                fragment = new AllNewsFragment(bundle.getString("type"));
                fragment.setArguments(bundle);
            }

            if (!fragment.isResumed()) {
                ft.setCustomAnimations(R.anim.fragment_in_anim,
                        R.anim.fragment_out_anim);
                ft.add(R.id.notification_container, fragment,
                        CLASSIFY_NOTIFICATIONS_TAG);
                ft.addToBackStack(CLASSIFY_NOTIFICATIONS_TAG);
            }
            break;

        case DETAILS_NOTIFICATION: // 消息详情页面
            fragment = fragmentManager
                    .findFragmentByTag(DETAILS_NOTIFICATION_TAG);
            if (fragment == null) {
                fragment = new NewsDetailsFragment();
                fragment.setArguments(bundle);
            }
            if (!fragment.isResumed()) {
                ft.setCustomAnimations(R.anim.fragment_in_anim,
                        R.anim.fragment_out_anim);
                ft.add(R.id.notification_container, fragment,
                        DETAILS_NOTIFICATION_TAG);
                ft.addToBackStack(DETAILS_NOTIFICATION_TAG);
            }
            break;
        }

        ft.commit();
    }

    private static Intent mUpdateIntent;

    /**
     * 返回上一层时更新UI的广播
     */
    public static void sendBordcastUpdateFragment(Context context) {
        if (mUpdateIntent == null) {
            mUpdateIntent = new Intent(
                    NotificationConstants.ACTION_FRAGMENT_UPDATE);
        }

        context.sendBroadcast(mUpdateIntent);
    }

    private static long lastClickTime;

    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 1000) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    /**
     * 判断当前的屏是园还是方形
     * @return
     */
    public static boolean IsCircularScreen(){
        return HardwareList.IsCircularScreen();
    }
}
