/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.notification.dao;

import java.io.Serializable;

/**
 * 分类消息
 *
 * @author tZhang
 */
public class Classification implements Serializable {

    private static final long serialVersionUID = 5111945837902813834L;

    public String type;

    public int number = 0;

    public int unread_num = 0;

    @Override
    public String toString() {
        return "[Notification: TYPE:" + type + " NUMBER:" + number
                + " UNREAD_NUM:" + unread_num + "]";
    }
}
