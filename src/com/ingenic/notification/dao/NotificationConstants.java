/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.notification.dao;

import android.net.Uri;

public class NotificationConstants {

    public static final String UUID = "5a177e43-82e6-d483-b7e3-d7072047e346";

    public static final String ACTION_NOTIFICATION_UPDATE = "com.ingenic.launcher.notification.NOTIFICATION_UPDATE";

    public static final String ACTION_FRAGMENT_UPDATE = "com.ingenic.launcher.notification.FRAGMENT_UPDATE";

    public static final Uri CONTENT_URI_NOTIFICATIONS = Uri
            .parse("content://com.ingenic.notification/notifications");

    public static final Uri CONTENT_URI_CALSSIFICATIONS = Uri
            .parse("content://com.ingenic.notification/calssifications");
}
