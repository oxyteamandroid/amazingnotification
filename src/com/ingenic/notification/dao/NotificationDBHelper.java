/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.notification.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.ingenic.iwds.datatransactor.elf.NotificationInfo;

/**
 * 消息中心数据库助手
 *
 * @author tZhang
 *
 */
public class NotificationDBHelper {
    private ContentResolver mResolver;
    private ContentValues mValues = new ContentValues();

    public NotificationDBHelper(Context context) {
        mResolver = context.getContentResolver();
    }

    /**
     * 插入一条通知到数据库
     *
     * @param info
     *            通知内容
     * @return 插入记录的ID
     */
    public long insertNotificationToDB(NotificationInfo info) {
        if ((mResolver != null) && (info != null)) {
            mValues.clear();
            mValues.put("appName", info.appName);
            mValues.put("packageName", info.packageName);
            mValues.put("title", info.title);
            mValues.put("message", info.content);
            mValues.put("time", info.updateTime);
            mValues.put("readed", 1); // 默认新收到的消息都是未读消息

            Uri uri = mResolver.insert(
                    NotificationConstants.CONTENT_URI_NOTIFICATIONS, mValues);

            return ContentUris.parseId(uri);
        }

        return -1;
    }

    /**
     * 插入一列通知信息到数据库
     *
     * @param notifications
     *            通知列表
     */
    public void insertNotificationListToDB(List<NotificationInfo> notifications) {
        if ((mResolver != null) && (notifications != null)
                && (notifications.size() > 0)) {
            for (NotificationInfo info : notifications) {
                mValues.clear();
                mValues.put("appName", info.appName);
                mValues.put("packageName", info.packageName);
                mValues.put("title", info.title);
                mValues.put("message", info.content);
                mValues.put("time", info.updateTime);
                mValues.put("readed", 1); // 默认新收到的消息都是未读消息
                mResolver.insert(
                        NotificationConstants.CONTENT_URI_NOTIFICATIONS,
                        mValues);
            }
        }
    }

    /**
     * 获取所有消息
     *
     * @param resolver
     * @return
     */
    public ArrayList<NotificationInfo> getNotificationForProvider() {
        if (mResolver == null) {
            return null;
        }

        ArrayList<NotificationInfo> result = new ArrayList<NotificationInfo>();

        Cursor cursor = mResolver.query(
                NotificationConstants.CONTENT_URI_NOTIFICATIONS, null, null,
                null, null);

        buildNotificationInfo(cursor, result);

        return result;
    }

    private void buildNotificationInfo(Cursor cursor,
            ArrayList<NotificationInfo> result) {
        if (cursor != null) {

            NotificationInfo notification = null;

            while (cursor.moveToNext()) {
                notification = new NotificationInfo();
                notification.id = cursor.getInt(0);
                notification.appName = cursor.getString(1);
                notification.packageName = cursor.getString(2);
                notification.title = cursor.getString(3);
                notification.content = cursor.getString(4);
                notification.updateTime = cursor.getLong(5);
                notification.read = cursor.getInt(6);
                result.add(notification);
                notification = null;
            }

            cursor.close();
        }
    }

    private static final int PAGE_COUNT = 100;
    private static final String ORDER_BY = BaseColumns._ID + " DESC LIMIT "
            + PAGE_COUNT + " OFFSET " + PAGE_COUNT + " * %1$d";

    /**
     * 分页查询消息记录
     *
     * @param pageIndex
     * @return 一页消息记录
     */
    public ArrayList<NotificationInfo> getNotificationPage(int pageIndex) {

        String sortOrder = String.format(ORDER_BY, pageIndex - 1);

        if (mResolver == null) {
            return null;
        }

        ArrayList<NotificationInfo> result = new ArrayList<NotificationInfo>();

        Cursor cursor = mResolver.query(
                NotificationConstants.CONTENT_URI_NOTIFICATIONS, null, null,
                null, sortOrder);

        buildNotificationInfo(cursor, result);

        return result;
    }

    /**
     * 获取所有分类
     *
     * @param resolver
     * @return
     */
    public ArrayList<Classification> getCalssificationForProvider() {
        if (mResolver == null) {
            return null;
        }

        String[] projection = { "A.TITLE", "A.NUMBER", "B.UNREAD_NUM" };

        Cursor cursor = mResolver.query(
                NotificationConstants.CONTENT_URI_CALSSIFICATIONS, projection,
                null, null, null);

        ArrayList<Classification> result = new ArrayList<Classification>();

        Classification classification = null;

        if (cursor != null) {
            while (cursor.moveToNext()) {
                classification = new Classification();
                classification.type = cursor.getString(0);
                classification.number = cursor.getInt(1);
                classification.unread_num = cursor.getInt(2);
                result.add(classification);
            }

            cursor.close();
        }

        return result;
    }

    /**
     * 分页获取某一分类的所有通知消息
     *
     * @param type
     *            消息类型
     * @param pageIndex
     *            页码
     * @return
     */
    public ArrayList<NotificationInfo> getNotificationPageByMessageType(
            String type, int pageIndex) {
        if (mResolver == null) {
            return null;
        }

        String sortOrder = String.format(ORDER_BY, pageIndex - 1);

        ArrayList<NotificationInfo> result = new ArrayList<NotificationInfo>();

        Cursor cursor = mResolver.query(
                NotificationConstants.CONTENT_URI_NOTIFICATIONS, null,
                "title=?", new String[] { type }, sortOrder);

        buildNotificationInfo(cursor, result);

        return result;
    }

    /**
     * 获取某一分类的所有通知消息
     *
     * @param resolver
     * @param messageType
     * @return
     */
    public ArrayList<NotificationInfo> getNotificationByMessageType(String type) {
        if (mResolver == null) {
            return null;
        }

        ArrayList<NotificationInfo> result = new ArrayList<NotificationInfo>();

        Cursor cursor = mResolver.query(
                NotificationConstants.CONTENT_URI_NOTIFICATIONS, null,
                "title=?", new String[] { type }, "readed DESC");

        buildNotificationInfo(cursor, result);

        return result;
    }

    /**
     * 根据Id删除某条通知消息
     *
     * @param id
     * @return
     */
    public int removeNotificationById(long id) {
        if (mResolver == null) {
            return -1;
        }

        return mResolver.delete(
                NotificationConstants.CONTENT_URI_NOTIFICATIONS, "_id=?",
                new String[] { id + "" });
    }

    /**
     * 删除某一分类的所有通知消息
     *
     * @param resolver
     * @param type
     * @return
     */
    public int removeNotificationByType(String type) {
        if (mResolver == null) {
            return -1;
        }

        return mResolver.delete(
                NotificationConstants.CONTENT_URI_NOTIFICATIONS, "title=?",
                new String[] { type });
    }

    /**
     * 删除所有通知消息
     *
     * @return
     */
    public int removeAllNotification() {
        if (mResolver == null) {
            return -1;
        }

        return mResolver.delete(
                NotificationConstants.CONTENT_URI_NOTIFICATIONS, null, null);
    }

    /**
     * 修改消息为已读
     *
     * @return
     */
    public int updateNotificationstatusToReadedById(long id) {
        if (mResolver == null) {
            return -1;
        }

        ContentValues values = new ContentValues();
        values.put("readed", 0); // 设置为已读

        return mResolver.update(
                NotificationConstants.CONTENT_URI_NOTIFICATIONS, values,
                "_id=?", new String[] { id + "" });
    }
}
