/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.notification.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenic.notification.R;

/**
 * 消息列表的Adapter
 *
 * @author tZhang
 */
public class NotificationAdapter extends BaseAdapter {

    private Context mContext;
    private List<HashMap<String, Object>> mList;
    private Date mDate = new Date();
    private SimpleDateFormat mSdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public NotificationAdapter(Context context,
            List<HashMap<String, Object>> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mList != null) {
            return mList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater inflater = LayoutInflater.from(mContext);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.notification_item_layout,
                    null);
            holder.icon = (ImageView) convertView
                    .findViewById(R.id.notification_icon);
            holder.title = (TextView) convertView
                    .findViewById(R.id.notification_title);
            holder.unread_num = (TextView) convertView
                    .findViewById(R.id.notification_unread_num);
            holder.date = (TextView) convertView
                    .findViewById(R.id.notification_date);
            holder.content = (TextView) convertView
                    .findViewById(R.id.notification_content_or_number);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (mList.size() > position) {
            if (mList.get(position).get("date") != null) {
                holder.unread_num.setVisibility(View.GONE);
                holder.date.setVisibility(View.VISIBLE);
                mDate.setTime(Long.parseLong(mList.get(position).get("date")
                        .toString()));
                holder.date.setText(mSdf.format(mDate));

                if ("iphone".equals(mList.get(position).get("packageName")
                        .toString())) {
                    if (Integer.valueOf(mList.get(position).get("isread")
                            .toString()) == 1) {
                        holder.icon
                                .setImageResource(R.drawable.iphone_msg_unread);
                    } else {
                        holder.icon
                                .setImageResource(R.drawable.iphone_msg_readed);
                    }
                } else {
                    if (Integer.valueOf(mList.get(position).get("isread")
                            .toString()) == 1) {
                        holder.icon
                                .setImageResource(R.drawable.android_msg_unread);
                    } else {
                        holder.icon
                                .setImageResource(R.drawable.android_msg_readed);
                    }
                }

            } else { // 显示的是分类消息界面
                int unread_num = Integer.parseInt(mList.get(position)
                        .get("unread_num").toString());
                if (unread_num != 0) {
                    holder.unread_num.setVisibility(View.VISIBLE);
                    holder.icon
                            .setImageResource(R.drawable.classify_msg_unread);
                } else {
                    holder.icon
                            .setImageResource(R.drawable.classify_msg_readed);
                    // holder.unread_num.setVisibility(View.GONE);
                }

                holder.unread_num.setText(unread_num + "");
                holder.date.setVisibility(View.GONE);

            }

            holder.title.setText(mList.get(position).get("title").toString());
            holder.content.setText(mList.get(position).get("content")
                    .toString());
        }

        return convertView;
    }

    private class ViewHolder {
        ImageView icon;
        TextView unread_num;
        TextView title;
        TextView date;
        TextView content;
    }

    public void onDeleteItem(View view, int position) {

        if (mList != null && mList.size() > position) {
            mList.remove(position);
            notifyDataSetChanged();
        }

    }

}
