/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.notification;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import com.ingenic.iwds.app.Note;
import com.ingenic.iwds.app.NotificationProxyServiceManager;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.notification.fragment.AllNewsFragment;
import com.ingenic.notification.util.NotificationUtil;

/**
 * 消息详情界面
 * 
 * @author tZhang
 */
public class NotificationActivity extends Activity implements
        ServiceClient.ConnectionCallbacks {

    private ServiceClient mClient;
    private NotificationProxyServiceManager mService;

    /**
     * 清除通知栏的Note
     */
    private Note mNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (mClient == null) {
            mClient = new ServiceClient(this,
                    ServiceManagerContext.SERVICE_NOTIFICATION_PROXY, this);
            mClient.connect();
        }

        mNote = new Note("Clear By PackageName", getPackageName());

        // 确保接收消息的服务被启动
        Intent intent = new Intent();
        intent.setClassName("com.ingenic.provider.notification.service",
                "NotificationService");
        startService(intent);

        if (savedInstanceState == null) {
            // 全部消息
            Fragment allNotification = new AllNewsFragment();
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.notification_container, allNotification,
                            NotificationUtil.ALL_NOTIFICATIONS_TAG).commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
        // 发送更新Fragment UI 的广播
        NotificationUtil.sendBordcastUpdateFragment(getApplicationContext());
    }

    @Override
    protected void onStart() {
        if (mService != null) {
            // 清除状态栏消息
            mService.notify(-1, mNote);
        }
        super.onStart();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            // 清除状态栏消息
            mService.notify(-1, mNote);
        }
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        mService = (NotificationProxyServiceManager) mClient
                .getServiceManagerContext();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
    }

}
