/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.notification.fragment;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.elf.NotificationInfo;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.widget.AbsListView;
import com.ingenic.iwds.widget.AbsListView.OnScrollListener;
import com.ingenic.iwds.widget.AdapterView;
import com.ingenic.iwds.widget.AmazingIndeterminateProgressBar;
import com.ingenic.iwds.widget.AmazingSwipeListView;
import com.ingenic.iwds.widget.AmazingSwipeListView.OnItemDeleteListener;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.iwds.widget.RightScrollView.OnRightScrollListener;
import com.ingenic.notification.R;
import com.ingenic.notification.adapter.NotificationAdapter;
import com.ingenic.notification.dao.NotificationConstants;
import com.ingenic.notification.dao.NotificationDBHelper;
import com.ingenic.notification.util.NotificationUtil;

/**
 * 显示所有消息/一个分类的所有消息的Fragment
 * 
 * @author tZhang
 */
public class AllNewsFragment extends Fragment implements OnRightScrollListener,
        OnItemDeleteListener, OnScrollListener,
        ServiceClient.ConnectionCallbacks {

    static final String TAG = "AllNewsFragment";

    private static final String UPDATE_LIST = "com.ingenic.action.update_list";
    public static final String BC_APPLE_CONNECT_STATE = "com.indroid.appleconnectstate";
    private static final int MSG_CONNECTED = 0;
    private static final int MSG_DISCONNECTED = 1;
    private static final int UPDATE_UI = 2;

    private ServiceClient mClient;
    private RightScrollView mView;
    private TextView mTitle;
    private AmazingSwipeListView mListView;
    private NotificationAdapter mAdapter;
    private View mHeaderView;
    private FragmentManager fragmentManager;
    private String mClassifyTitle = "";
    private boolean isFromClassify = false;
    private boolean isComeBackFromClassify = false;
    private ArrayList<NotificationInfo> mNotifications;
    private View mEmptyView;
    private NotificationDBHelper mDbHelper;
    private SharedPreferences mPreferences;
    private View mLoadView;
    private AmazingIndeterminateProgressBar mLoadingProgress;
    private TextView mLoadingText;
    private boolean mAndroidConnState = false;
    private boolean mAncsConnState = false;

    public AllNewsFragment() {}

    public AllNewsFragment(String title) {
        mClassifyTitle = title;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        mPreferences = getActivity().getSharedPreferences("notifications",
                Context.MODE_PRIVATE);

        String allNotifications = getActivity().getResources().getString(
                R.string.all_notifications);
        if (null == mClassifyTitle) {
            mClassifyTitle = allNotifications;
            isComeBackFromClassify = true;
        } else if ("".equals(mClassifyTitle)) {
            mClassifyTitle = allNotifications;
        } else {
            isFromClassify = true;
        }

        if (!(isFromClassify || isComeBackFromClassify)) {
            mClient = new ServiceClient(getActivity(),
                    ServiceManagerContext.SERVICE_CONNECTION, this);
            mClient.connect();

            mAncsConnState = Settings.System.getInt(getActivity()
                    .getApplication().getContentResolver(),
                    "ANCS_CONNECT_STATE", 0) == 1 ? true : false;
        }

        fragmentManager = getFragmentManager();

        mView = new RightScrollView(getActivity());
        mView.setContentView(R.layout.fragment_list_layout);
        mView.setOnRightScrollListener(this);

        mListView = (AmazingSwipeListView) mView
                .findViewById(R.id.notification_list);

        // 设置分隔线
        mListView.setDivider(getResources().getDrawable(R.drawable.line));
        mListView.setHeaderDividersEnabled(true);
        mListView.setFooterDividersEnabled(true);
        mListView.setDividerHeight(1);

        // 设置进入动画
        mListView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(
                getActivity(), R.anim.listview_layout_anim));

        mEmptyView = mView.findViewById(R.id.emptyview);
        mHeaderView = inflater.inflate(R.layout.notification_headview_layout,
                null);

        if (!isFromClassify) {
            mHeaderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!NotificationUtil.isFastDoubleClick()
                            && mNotifications != null
                            && mNotifications.size() > 0) {
                        // 切换到分类消息视图
                        NotificationUtil.switchToFragment(fragmentManager,
                                NotificationUtil.ALL_CLASSIFICATIONS,
                                emptyBundle);
                    }
                }
            });
        }

        mListView.setSelector(R.drawable.list_selector);
        // mListView.setEmptyView(mEmptyView);
        mListView.setOnItemDeleteListener(this);
        mListView.setOnScrollListener(this);

        // 注册更新界面的广播
        if (mNotificationReceiver != null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(NotificationConstants.ACTION_NOTIFICATION_UPDATE);
            filter.addAction(NotificationConstants.ACTION_FRAGMENT_UPDATE);
            filter.addAction(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS);
            filter.addAction(ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS);
            filter.addAction(BC_APPLE_CONNECT_STATE);
            filter.addAction(UPDATE_LIST);
            getActivity().registerReceiver(mNotificationReceiver, filter);
        }

        mLoadView = inflater.inflate(R.layout.load_view_layout, null);
        mDbHelper = new NotificationDBHelper(getActivity());

        if (isFromClassify || isComeBackFromClassify) {
            // 初始化界面
            initNewsClassify();
        }

        return mView;
    }

    private void initNewsClassify() {

        // 加载数据
        loadData();

        if (mNotifications != null && mNotifications.size() > 0) {
            loadListViewItem();
        } else {
            if (mEmptyView != null) {
                // 没有数据
                if (mListView.getEmptyView() == null) {
                    mListView.setEmptyView(mEmptyView);
                }
                ((TextView) mEmptyView).setText(R.string.no_notifications);
            }
        }

    }

    private NotificationInfo showInfo;

    private void loadListViewItem() {
        // add header
        mTitle = (TextView) mHeaderView.findViewById(R.id.title);
        mTitle.setText(mClassifyTitle);
        if(mListView.getHeaderViewsCount() == 0){
            mListView.addHeaderView(mHeaderView, null, true);
        }

        // add item
        loadItem();

        mAdapter = new NotificationAdapter(getActivity(), mList);
        mListView.setAdapter(mAdapter);

        // set item click
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            private NotificationInfo info;

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                if (!NotificationUtil.isFastDoubleClick()
                        && mNotifications != null
                        && position < mNotifications.size()) {

                    if (mView != null) {
                        mView.disableRightScroll();
                    }

                    info = mNotifications.get(position);
                    showInfo = info;

                    // 标记为已读
                    info.read = 0;

                    mPreferences
                            .edit()
                            .putInt("all_current_position",
                                    mListView.getFocusPosition()).commit();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("notification", info);
                    // 切换到消息详情界面
                    NotificationUtil.switchToFragment(fragmentManager,
                            NotificationUtil.DETAILS_NOTIFICATION, bundle);
                }
            }

        });

        if(mListView.getFooterViewsCount() == 0){
            // add footer
            mListView.addFooterView(mLoadView);
        }
    }

    private Bundle emptyBundle = new Bundle();
    private ArrayList<HashMap<String, Object>> mList = new ArrayList<HashMap<String, Object>>();

    /**
     * 从数据库获取数据
     */
    private void loadData() {

        // get item data
        if (!isFromClassify) {
            // mNotifications = mDbHelper.getNotificationForProvider();
            mNotifications = mDbHelper.getNotificationPage(pageIndex);
        } else {
            mNotifications = mDbHelper.getNotificationPageByMessageType(
                    mClassifyTitle, pageIndex);
        }
    }

    /**
     * 加载Item
     */
    private void loadItem() {
        mList.clear();

        if (mNotifications == null) {
            return;
        }

        for (NotificationInfo notification : mNotifications) {
            HashMap<String, Object> map = new HashMap<String, Object>();

            map.put("packageName", notification.packageName);
            map.put("title", notification.title);
            map.put("appName", notification.appName);
            map.put("content", notification.content);
            map.put("date", notification.updateTime);
            map.put("isread", notification.read);
            mList.add(map);
        }

        mLoadingProgress = (AmazingIndeterminateProgressBar) mLoadView
                .findViewById(R.id.load_progress);
        mLoadingText = (TextView) mLoadView.findViewById(R.id.load_text);
        if (mNotifications != null && mNotifications.size() <= 2) {
            mLoadingProgress.setVisibility(View.GONE);
            mLoadingText.setVisibility(View.GONE);
        } else {
            mLoadingProgress.setVisibility(View.VISIBLE);
            mLoadingText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRightScroll() {
        Activity activity = getActivity();
        if (null != activity) {
            // 退出/返回
            if(isFromClassify){
                activity.onBackPressed();
            } else {
                activity.finish();
            }
        }
    }

    @Override
    public void onDelete(View view, int position) {
        if (mNotifications == null) return;

        if (position >= 0 && mNotifications.size() > position) {
            mDbHelper.removeNotificationById(mNotifications.get(position).id);
            mNotifications.remove(position);// 删除本地
            mAdapter.onDeleteItem(view, position);

            if (mAdapter.getCount() == 0) {
                if (isFromClassify) {
                    getActivity().onBackPressed();
                } else {
                    if (mListView.getEmptyView() == null) {
                        mListView.setEmptyView(mEmptyView);
                    }
                    ((TextView) mEmptyView).setText(R.string.no_notifications);
                }
            }

            if (mNotifications != null && mNotifications.size() <= 2) {
                mLoadingProgress.setVisibility(View.GONE);
                mLoadingText.setVisibility(View.GONE);
            } else {
                mLoadingProgress.setVisibility(View.VISIBLE);
                mLoadingText.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mNotificationReceiver != null) {
            getActivity().unregisterReceiver(mNotificationReceiver);
        }
    }

    private boolean isFromFragmentUpdate;

    /**
     * 接收到新的消息更新UI
     */
    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {

        private String action;

        @Override
        public void onReceive(Context context, Intent intent) {

            action = intent.getAction();

            if (NotificationConstants.ACTION_NOTIFICATION_UPDATE.equals(action)) {

                NotificationInfo info = intent
                        .getParcelableExtra("notification");

                if (mNotifications == null) {
                    mNotifications = new ArrayList<NotificationInfo>();
                }

                if (isFromClassify) {
                    if (!mClassifyTitle.equals(info.title)) {
                        return;
                    }
                }

                info.read = 1;
                info.id = intent.getLongExtra("_id", -1);
                mNotifications.add(0, info);

                isFromFragmentUpdate = false;
                uiHandle.sendEmptyMessage(UPDATE_UI);
            } else if (NotificationConstants.ACTION_FRAGMENT_UPDATE
                    .equals(action)) {
                if (mView != null) {
                    mView.enableRightScroll();
                }

                isFromFragmentUpdate = true;
                uiHandle.sendEmptyMessage(UPDATE_UI);
            } else if (UPDATE_LIST.equals(action)) {// 在详情页面删除消息后刷新页面
                if (showInfo != null && mNotifications != null) {
                    mNotifications.remove(showInfo);
                    isFromFragmentUpdate = true;
                    uiHandle.sendEmptyMessage(UPDATE_UI);
                }
            } else if (ConnectionServiceManager.ACTION_CONNECTED_ADDRESS
                    .equals(action)) {
                mAndroidConnState = true;
                mPreferences.edit()
                        .putBoolean("android_conn_state", mAndroidConnState)
                        .commit();

                if (!mAncsConnState) {
                    isFromFragmentUpdate = false;
                    uiHandle.sendEmptyMessage(UPDATE_UI);
                }

                // Android连接，提示用户
                AmazingToast.showToast(getActivity(), R.string.reconnect_tip,
                        AmazingToast.LENGTH_LONG, AmazingToast.BOTTOM_CENTER);
            } else if (ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS
                    .equals(action)) {
                mAndroidConnState = false;
                mPreferences.edit()
                        .putBoolean("android_conn_state", mAndroidConnState)
                        .commit();

                if (!mAncsConnState) {
                    if (mListView.getEmptyView() == null) {
                        mListView.setEmptyView(mEmptyView);
                    }
                    mEmptyView.setVisibility(View.VISIBLE);
                    ((TextView) mEmptyView).setText(R.string.unlink_tip);
                }

                // Android断连，提示用户
                AmazingToast.showToast(getActivity(),
                        R.string.devices_disconnected,
                        AmazingToast.LENGTH_LONG, AmazingToast.BOTTOM_CENTER);

                if (isFromClassify) {
                    getActivity().onBackPressed();
                }
            } else if (BC_APPLE_CONNECT_STATE.equals(action)) {// ancs连接状态改变
                String connState = intent.getStringExtra("connState");
                if ("disconnected".equals(connState)) {
                    mAncsConnState = false;

                    if (!mAndroidConnState) {
                        if (mListView.getEmptyView() == null) {
                            mListView.setEmptyView(mEmptyView);
                        }
                        mEmptyView.setVisibility(View.VISIBLE);
                        ((TextView) mEmptyView).setText(R.string.unlink_tip);
                    }

                    // APPLE断连，提示用户
                    AmazingToast.showToast(getActivity(),
                            R.string.disconnected_apple_tip,
                            AmazingToast.LENGTH_LONG,
                            AmazingToast.BOTTOM_CENTER);

                    if (isFromClassify) {
                        getActivity().onBackPressed();
                    }
                } else if ("connected".equals(connState)) {
                    mAncsConnState = true;

                    if (!mAndroidConnState) {
                        isFromFragmentUpdate = true;
                        uiHandle.sendEmptyMessage(UPDATE_UI);
                    }

                    // 连接APPLE，提示用户
                    AmazingToast.showToast(getActivity(),
                            R.string.connected_apple_tip,
                            AmazingToast.LENGTH_LONG,
                            AmazingToast.BOTTOM_CENTER);
                }
            }

        }
    };

    private Handler uiHandle = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_CONNECTED:
                // 初始化界面
                mAndroidConnState = true;
                initNewsClassify();
                break;
            case MSG_DISCONNECTED:
                mAndroidConnState = false;
                if (mAncsConnState) {
                    // 初始化界面
                    initNewsClassify();
                } else {
                    if (mEmptyView != null) {
                        // 没有连接
                        if (mListView.getEmptyView() == null) {
                            mListView.setEmptyView(mEmptyView);
                        }
                        ((TextView) mEmptyView).setText(R.string.unlink_tip);
                    }
                }
                break;
            case UPDATE_UI:
                if (mAdapter == null) {
                    // 重新初始化
                    initNewsClassify();
                    if(mAdapter == null) return;
                } else {
                    // 重新读取列表数据
                    loadItem();
                }

                // 更新界面数据
                if (mAdapter.getCount() == 0) {
                    if (mListView.getEmptyView() == null) {
                        mListView.setEmptyView(mEmptyView);
                    }
                    ((TextView) mEmptyView).setText(R.string.no_notifications);
                } else {
                    if (isFromFragmentUpdate) {
                        mListView.setFocusPosition(mPreferences.getInt(
                                "current_position", 0));
                    } else {
                        mListView.setFocusPosition(mListView
                                .getLastVisiblePosition());
                    }
                }

                // 刷新适配器，更新界面
                mAdapter.notifyDataSetChanged();
                break;
            default:
                break;
            }
        };
    };

    private boolean isLoading = false;
    private int pageIndex = 1;
    private int lastVisiblePosition = 0;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

        if (scrollState == OnScrollListener.SCROLL_STATE_IDLE
                && mNotifications.size() + 1 == lastVisiblePosition) {

            if (!isLoading) {
                pageIndex++;
                new LoadTask().execute(pageIndex);
            }
        }

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        lastVisiblePosition = firstVisibleItem + 2;
    }

    private class LoadTask extends
            AsyncTask<Integer, Void, ArrayList<NotificationInfo>> {
        @Override
        protected ArrayList<NotificationInfo> doInBackground(Integer... params) {
            if (!isFromClassify) {
                return mDbHelper.getNotificationPage(params[0]);
            } else {
                return mDbHelper.getNotificationPageByMessageType(
                        mClassifyTitle, params[0]);
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
        };

        @Override
        protected void onPostExecute(ArrayList<NotificationInfo> result) {
            super.onPostExecute(result);

            if (result.size() == 0) {
                mListView.removeFooterView(mLoadView);
                return;
            }

            int currentSelection = mNotifications.size() - 1;
            mNotifications.addAll(result);
            loadItem();
            mAdapter.notifyDataSetChanged();
            isLoading = false;
            mListView.setSelection(currentSelection);
        }
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        ConnectionServiceManager manager = (ConnectionServiceManager) serviceClient
                .getServiceManagerContext();
        boolean connState = false;
        if (manager.getConnectedDeviceDescriptors().length != 0) {
            uiHandle.sendEmptyMessage(MSG_CONNECTED);
            connState = true;
        } else {
            uiHandle.sendEmptyMessage(MSG_DISCONNECTED);
        }
        mPreferences.edit().putBoolean("android_conn_state", connState);
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
    }

}
