/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.notification.fragment;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.widget.AdapterView;
import com.ingenic.iwds.widget.AmazingSwipeListView;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.AmazingSwipeListView.OnItemDeleteListener;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.iwds.widget.RightScrollView.OnRightScrollListener;
import com.ingenic.notification.R;
import com.ingenic.notification.adapter.NotificationAdapter;
import com.ingenic.notification.dao.Classification;
import com.ingenic.notification.dao.NotificationConstants;
import com.ingenic.notification.dao.NotificationDBHelper;
import com.ingenic.notification.util.NotificationUtil;

/**
 * 显示消息分类的Fragment
 *
 * @author tZhang
 */
public class NewsClassifyFragment extends Fragment implements
        OnRightScrollListener, OnItemDeleteListener {

    private static final int UPDATE_UI = 100;

    private RightScrollView mView;

    private TextView title;

    private AmazingSwipeListView mListView;

    private NotificationAdapter mAdapter;

    private Context mContext;

    private View headerView;

    private FragmentManager fragmentManager;

    private ArrayList<Classification> classifications;

    private NotificationDBHelper mDbHelper;

    private View mEmptyView;

    private SharedPreferences mPreferences;

    private AmazingDialog mDeleteAlldialog;
    private String type;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        mContext = getActivity();

        mPreferences = mContext.getSharedPreferences("notifications",
                Context.MODE_PRIVATE);

        fragmentManager = getFragmentManager();

        mView = new RightScrollView(mContext);
        mView.setContentView(R.layout.fragment_list_layout);
        mView.setOnRightScrollListener(this);

        mEmptyView = mView.findViewById(R.id.emptyview);
        mListView = (AmazingSwipeListView) mView
                .findViewById(R.id.notification_list);

        // 设置分隔线
        mListView.setDivider(getResources().getDrawable(R.drawable.line));
        mListView.setHeaderDividersEnabled(true);
        mListView.setFooterDividersEnabled(true);
        mListView.setDividerHeight(1);

        // 设置进入动画
        mListView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(
                getActivity(), R.anim.listview_layout_anim));

        mListView.setOnItemDeleteListener(this);
        mListView.setEmptyView(mEmptyView);

        headerView = inflater.inflate(R.layout.notification_headview_layout,
                null);

        initNewsClassify(inflater);

        return mView;
    }

    private ArrayList<HashMap<String, Object>> mList;

    private void initNewsClassify(LayoutInflater inflater) {

        if (mNotificationReceiver != null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(NotificationConstants.ACTION_NOTIFICATION_UPDATE);
            filter.addAction(NotificationConstants.ACTION_FRAGMENT_UPDATE);
            filter.addAction(ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS);
            filter.addAction(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS);
            filter.addAction(AllNewsFragment.BC_APPLE_CONNECT_STATE);
            mContext.registerReceiver(mNotificationReceiver, filter);
        }

        mDbHelper = new NotificationDBHelper(mContext);

        // get item data
        loadData();

        // add item
        mList = new ArrayList<HashMap<String, Object>>();
        if (classifications != null && classifications.size() > 0) {
            loadListViewItem();
        } else {
            if (mEmptyView != null) {
                // 没有数据
                ((TextView) mEmptyView).setText(R.string.no_notifications);
            }
        }

        mDeleteAlldialog = new AmazingDialog(mContext).setNegativeButton(0,
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mDeleteAlldialog.dismiss();
                    }
                });

    }

    private void loadListViewItem() {
        // add header
        mListView.addHeaderView(headerView, null, true);

        loadItem();

        mAdapter = new NotificationAdapter(mContext, mList);
        mListView.setAdapter(mAdapter);
        mListView.setSelector(R.drawable.list_selector);

        // set view
        title = (TextView) headerView.findViewById(R.id.title);
        title.setText(R.string.all_classifications);

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NotificationUtil.isFastDoubleClick()) {
                    // 切换到所有消息视图
                    NotificationUtil.switchToFragment(fragmentManager,
                            NotificationUtil.ALL_NOTIFICATIONS, null);
                }
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                if (!NotificationUtil.isFastDoubleClick()
                        && position < classifications.size()) {
                    if (mView != null) {
                        mView.disableRightScroll();
                    }

                    mPreferences
                            .edit()
                            .putInt("current_position",
                                    mListView.getFocusPosition()).commit();
                    Bundle bundle = new Bundle();
                    bundle.putString("type", classifications.get(position).type);
                    // 切换到对应分类的消息Fragment
                    NotificationUtil.switchToFragment(fragmentManager,
                            NotificationUtil.CLASSIFY_NOTIFICATIONS, bundle);
                }
            }
        });

        // add footer
        View footer = new View(mContext);
        mListView.addFooterView(footer);
    }

    private void loadData() {
        classifications = mDbHelper.getCalssificationForProvider();
    }

    private void loadItem() {
        mList.clear();

        for (int i = 0; i < classifications.size(); i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            Classification classification = classifications.get(i);
            map.put("title", classification.type);
            map.put("unread_num", classification.unread_num);
            map.put("content", String.format(
                    mContext.getString(R.string.classify_num),
                    classification.number));
            mList.add(map);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mNotificationReceiver != null) {
            mContext.unregisterReceiver(mNotificationReceiver);
        }
    }

    @Override
    public void onRightScroll() {
        Activity activity = getActivity();
        if (null != activity) {
            // 退出
            activity.finish();
        }
    }

    @Override
    public void onDelete(final View view, final int position) {
        if (position < 0 || position >= classifications.size()) {
            return;
        }

        type = classifications.get(position).type;

        mDeleteAlldialog
                .setContent(
                        String.format(
                                mContext.getString(R.string.delete_all_msg),
                                type)).setPositiveButton(0,
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                classifications.remove(position);
                                mDbHelper.removeNotificationByType(type);
                                mAdapter.onDeleteItem(view, position);

                                if (mAdapter.getCount() == 0) {
                                    ((TextView) mEmptyView)
                                            .setText(R.string.no_notifications);
                                    mAdapter.notifyDataSetChanged();
                                }

                                mDeleteAlldialog.dismiss();
                            }
                        });

        mDeleteAlldialog.show();
    }

    private boolean isFromFragmentUpdate;

    /**
     * 接收到新的消息更新UI
     */
    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (NotificationConstants.ACTION_NOTIFICATION_UPDATE.equals(action)) {
                isFromFragmentUpdate = false;
                uiHandle.sendEmptyMessage(UPDATE_UI);
            } else if (NotificationConstants.ACTION_FRAGMENT_UPDATE.equals(action)) {
                if (mView != null) {
                    mView.enableRightScroll();
                }
                isFromFragmentUpdate = true;
                uiHandle.sendEmptyMessage(UPDATE_UI);
            } else if (ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS.equals(action)) {
                // Android连接断开，提示用户
                AmazingToast.showToast(getActivity(),
                        R.string.devices_disconnected,
                        AmazingToast.LENGTH_LONG, AmazingToast.BOTTOM_CENTER);

                mPreferences.edit().putBoolean("android_conn_state", false).commit();

                boolean ancsConnState = Settings.System.getInt(getActivity()
                        .getApplication().getContentResolver(),
                        "ANCS_CONNECT_STATE", 0) == 1 ? true : false;
                if (!ancsConnState) {
                    // 没有连接
                    mEmptyView.setVisibility(View.VISIBLE);
                    ((TextView) mEmptyView).setText(R.string.unlink_tip);
                }
            } else if (ConnectionServiceManager.ACTION_CONNECTED_ADDRESS.equals(action)) {
                mEmptyView.setVisibility(View.GONE);

                mPreferences.edit().putBoolean("android_conn_state", true).commit();

                // 连接Android，提示用户
                AmazingToast.showToast(getActivity(), R.string.reconnect_tip,
                        AmazingToast.LENGTH_LONG, AmazingToast.BOTTOM_CENTER);
            } else if (AllNewsFragment.BC_APPLE_CONNECT_STATE.equals(action)) {
                String connState = intent.getStringExtra("connState");
                if ("disconnected".equals(connState)) {
                    // APPLE断连，提示用户
                    AmazingToast.showToast(getActivity(),
                            R.string.disconnected_apple_tip,
                            AmazingToast.LENGTH_LONG,
                            AmazingToast.BOTTOM_CENTER);

                    boolean androidConnState = mPreferences.getBoolean("android_conn_state", false);

                    if (!androidConnState) {
                        // 没有连接
                        mEmptyView.setVisibility(View.VISIBLE);
                        ((TextView) mEmptyView).setText(R.string.unlink_tip);
                    }
                } else if ("connected".equals(connState)) {
                    mEmptyView.setVisibility(View.GONE);

                    // 连接APPLE，提示用户
                    AmazingToast.showToast(getActivity(),
                            R.string.connected_apple_tip,
                            AmazingToast.LENGTH_LONG,
                            AmazingToast.BOTTOM_CENTER);
                }
            }

        }
    };

    private Handler uiHandle = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == UPDATE_UI && mAdapter != null) {
                loadData(); // 重新加载数据

                if (mAdapter == null) {
                    loadListViewItem();
                } else {
                    loadItem();
                }

                if (mAdapter.getCount() == 0) {
                    ((TextView) mEmptyView).setText(R.string.no_notifications);
                } else {
                    if (isFromFragmentUpdate) {
                        mListView.setFocusPosition(mPreferences.getInt(
                                "current_position", 0));
                    } else {
                        mListView.setSelection(0);
                    }
                }

                mAdapter.notifyDataSetChanged();
            }
        };
    };
}
