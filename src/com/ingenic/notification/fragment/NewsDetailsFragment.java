/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotification/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.notification.fragment;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.HardwareList;
import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.datatransactor.elf.NotificationInfo;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.iwds.widget.RightScrollView.OnRightScrollListener;
import com.ingenic.notification.R;
import com.ingenic.notification.dao.NotificationDBHelper;
import com.ingenic.notification.util.NotificationUtil;

/**
 * 显示消息详情的Fragment
 *
 * @author tZhang
 */
public class NewsDetailsFragment extends Fragment implements
        OnRightScrollListener {

    static final String TAG = "NewsDetailsFragment";

    private RightScrollView mView;

    /**
     * 删除消息按钮
     */
    private Button delete_btn;

    /**
     * 消息ID
     */
    private long id;

    /**
     * 消息标题
     */
    private TextView title;

    /**
     * 消息时间
     */
    private TextView time;

    /**
     * 消息内容
     */
    private TextView message;

    /**
     * 消息中心的数据库助手
     */
    private NotificationDBHelper mDbHelper;

    /**
     * 消息的时间格式
     */
    private Date date = new Date();
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    /**
     * 删除消息的Dialog
     */
    private AmazingDialog dialog_del;

    /**
     * 刷新界面
     */
    private Intent intent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        // 初始化数据库助手
        mDbHelper = new NotificationDBHelper(getActivity());

        // 获取参数
        Bundle bundle = getArguments();
        NotificationInfo notification = (NotificationInfo) bundle
                .getParcelable("notification");

        // 设置右滑View
        mView = new RightScrollView(getActivity());
        if(HardwareList.IsCircularScreen()){
            mView.setContentView(R.layout.news_details_fragment_layout_round);
        } else {
            mView.setContentView(R.layout.news_details_fragment_layout);
        }
        mView.setOnRightScrollListener(this);

        // 消息标题
        title = (TextView) mView.findViewById(R.id.msg_title);

        // 时间
        time = (TextView) mView.findViewById(R.id.msg_time);

        // 消息内容
        message = (TextView) mView.findViewById(R.id.msg);

        if (notification != null) {
            id = notification.id;
            title.setText(notification.title);
            date.setTime(notification.updateTime);
            time.setText(sdf.format(date));
            message.setText(notification.content);

            // 更改数据库
            mDbHelper.updateNotificationstatusToReadedById(id);
        }

        // 删除按钮
        delete_btn = (Button) mView.findViewById(R.id.msg_del);

        intent = new Intent("com.ingenic.action.update_list");

        // 确认删除对话框
        dialog_del = new AmazingDialog(getActivity())
                .setContent(R.string.delete_msg)
                .setPositiveButton(0, new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        if (mDbHelper != null) {
                            mDbHelper.removeNotificationById(id);

                            getActivity().sendBroadcast(intent);

                            AmazingToast
                                    .showToast(getActivity(),
                                            R.string.delete_success,
                                            Toast.LENGTH_SHORT);
                            dialog_del.dismiss();
                            getActivity().onBackPressed();

                        }
                    }
                }).setNegativeButton(0, new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        dialog_del.dismiss();
                    }
                });

        // 设置删除按钮点击监听
        delete_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (!NotificationUtil.isFastDoubleClick()) {
                    dialog_del.show();
                }
            }
        });
        return mView;
    }

    @Override
    public void onRightScroll() {
        Activity activity = getActivity();
        if (null != activity) {
            // 返回
            activity.onBackPressed();
        }
    }
}
